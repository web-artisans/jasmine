FROM node:7.1-alpine

RUN npm install -g jasmine

RUN mkdir /project

VOLUME /project

ADD test.sh /

ENTRYPOINT ash /test.sh